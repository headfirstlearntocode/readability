import ch1text

# Funcion para calcular el total de sentencias en el texto
def count_sentences(text):
    # Computing total sentences
    count = 0
    # terminals = [".", ";", "!", "?"]
    terminals = '.;!?'
    for char in text:
        if char in terminals:
            count += 1
    return count


# Funcion para calcular el total de silabas en una palabra
def count_syllables_in_word(word):
    count = 0

    # Heuristics
    # If a word has fewer than 3 characters, then count it as one syllable
    if len(word) <= 3:
        return 1

    vowels = 'aeiouAEIOU'
    # For words with a consecutive syllable
    previous_char_was_vowel = False
    for char in word:
        if char in vowels:
            if not previous_char_was_vowel:
                count += 1
            previous_char_was_vowel = True
        else:
            previous_char_was_vowel = False

    return count



# Funcion para calcular el total de silabas en un listado de palabras
def count_syllables(words):
    count = 0

    for word in words:
        word_count = count_syllables_in_word(word)
        count += word_count
    return count    


# Funcion que llama a las demas funciones e imprime el total de words, sentences, syllables
def comput_readability(text):
    total_words = 0
    total_sentences = 0
    total_syllables = 0
    score = 0

    # Computing total words
    words = text.split()
    total_words = len(words)
    
    # Computing total sentences
    total_sentences = count_sentences(text)
    # Computing total syllables
    total_syllables = count_syllables(words)

    # Imprimiendo totales
    print(total_words, "words")
    print(total_sentences, "sentences")
    print(total_syllables, "syllables")


# Llamando a la funcion comput_readability
comput_readability(ch1text.text)
